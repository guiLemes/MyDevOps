import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

class App extends Component {

  botaoSubmit = e => {
    e.preventDefault();
    if (
      e.target.email.value === "eduardo.lino@pucpr.br" &&
      e.target.senha.value === "123456"
    ) {
      this.setState({message:"Acessado com sucesso!"});
      e.target.email.value = "";
      e.target.senha.value = "";
    } else {
      this.setState({message:"Usuário ou senha incorretos!"});
    }
  };

  constructor(props) {
      super(props);
      this.state= {
        message: ""};
    }

  render() {
    return (
      <div className="App" >
        <h1>Login</h1>
        <form className="form" onSubmit={this.botaoSubmit}>
          <div className="input-group">
            <input type="email" name="email"/>
          </div>
          <div className="input-group">
            <input type="password" name="senha" />
          </div>
          <button className="primary">Acessar</button>
        </form>
        <label>{this.state.message}</label>
      </div>
    );
  }
}


export default App;
